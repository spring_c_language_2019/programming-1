#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

#define High 15  
#define Width 20

int ball_x,ball_y; 
int ball_vx,ball_vy; 
int canvas[High][Width] = {0}; 

void gotoxy(int x,int y)  
{
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X = x;
    pos.Y = y;
    SetConsoleCursorPosition(handle,pos);
}

void startup()  
{
	ball_x = 0;
	ball_y = Width/2;
	ball_vx = 1;
	ball_vy = 1;
	canvas[ball_x][ball_y] = 1;	
	}

void show()  
{
	gotoxy(0,0);    
	int i,j;
	for (i=0;i<High;i++)
	{
		for (j=0;j<Width;j++)
		{
			if (canvas[i][j]==0)
				printf(" ");   
			else if (canvas[i][j]==1)
				printf("0");   
				}
		printf("|\n"); 
	}
	for (j=0;j<Width;j++)
		printf("-"); 
		}	

void updateWithoutInput()  
{
	canvas[ball_x][ball_y] = 0;		
	
		ball_x = ball_x + ball_vx;
		ball_y = ball_y + ball_vy;		
		if ((ball_x==0)||(ball_x==High-2))
			ball_vx = -ball_vx;
		if ((ball_y==0)||(ball_y==Width-1))
			ball_vy = -ball_vy;			
			canvas[ball_x-1][ball_y] = 1;
			Sleep(50);
		}
		void updateWithInput()  
{	
}
int main()
{
	startup(); 	
	while (1)  
	{
		show();  
		updateWithoutInput();  
		updateWithInput();     
	}
	return 0;
}

