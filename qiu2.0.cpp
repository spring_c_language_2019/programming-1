#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

#define High 15  
#define Width 20

// 全局变量
int ball_x,ball_y; 
int ball_vx,ball_vy; 
int position_x,position_y; 
int ridus;  
int left,right; 
int canvas[High][Width] = {0}; 

void gotoxy(int x,int y)  
{
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X = x;
    pos.Y = y;
    SetConsoleCursorPosition(handle,pos);
}

void startup()  
{
	ball_x = 0;
	ball_y = Width/2;
	ball_vx = -1;
	ball_vy = 1;
	canvas[ball_x][ball_y] = 1;	
	ridus = 5;
	position_x = High-1;
	position_y = Width/2;
	left = position_y - ridus;
	right = position_y + ridus;
	int k;
	for (k=left;k<=right;k++)  
		canvas[position_x][k] = 2;	
		}

void show()
{
	gotoxy(0,0);
	int i,j;
	for (i=0;i<High;i++)
	{
		for (j=0;j<Width;j++)
		{
			if (canvas[i][j]==0)
			printf(" ");
			else if (canvas[i][j]==1)
			printf(" 0 ");
			else if (canvas[i][j]==2)
			printf (" * ");
		}
		printf (" |\n ");
	}
	for (j=0;j<Width;j++)
	printf(" - ");
	printf(" \n ");
  }  
  
  void updateWithoutInput ()
  {
  	if (ball_x==High-2)
	{
		if ( (ball_y>=left) && (ball_y<=right) )   
		{			
		}
		else    
		{
			printf("游戏失败\n");
			system("pause");
			exit(0);
		}	
	}
	canvas[ball_x][ball_y] = 0;		
		
		ball_x = ball_x + ball_vx;
		ball_y = ball_y + ball_vy;	
		
		if ((ball_x==0)||(ball_x==High-2))
			ball_vx = -ball_vx;
		if ((ball_y==0)||(ball_y==Width-1))
			ball_vy = -ball_vy;	
			
			canvas[ball_x][ball_y] = 1;
			Sleep(50);
  }
  void updateWithInput()  
{	
	char input;
	if(kbhit())  
	{
		input = getch();  
		if (input == 'a' && left>0)   
		{
			canvas[position_x][right] = 0;
			position_y--;  
			left = position_y - ridus;
			right = position_y + ridus;
			canvas[position_x][left] = 2;
		}
		if (input == 'd' && right<Width-1)
		{
			canvas[position_x][left] = 0;
			position_y++;  
			left = position_y - ridus;
			right = position_y + ridus;
			canvas[position_x][right] = 2;
		}
	}
}

int main()
{
	startup();  
	while (1)  
	{
		show();  
		updateWithoutInput();  
		updateWithInput();     
	}
	return 0;
}
		

