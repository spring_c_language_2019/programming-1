#include <stdio.h>  
#include <conio.h>
#include<stdlib.h> 

int map[9][11] = {
    {0,1,1,1,1,1,1,1,1,1,0},  //0代表空地
    {0,1,0,0,0,1,0,0,0,1,0},  //1代表墙
    {0,1,0,4,4,4,4,4,0,1,0},  //3代表目的地
    {0,1,0,4,0,4,0,4,0,1,1},  //4代表箱子
    {0,1,0,0,0,0,0,0,4,0,1},  //5代表人 
    {1,1,0,1,1,1,1,0,4,0,1},
    {1,0,8,3,3,3,3,1,0,0,1},  //2 3 4 5 6 7 8 9 1 0
    {1,0,3,3,3,3,3,0,0,1,1},
    {1,1,1,1,1,1,1,1,1,1,0} };

//绘制地图  //二维数组+switch()
void DrawMap()
{
    //遍历二维数组  //0 打印空格  //1 墙   //3 目的地  //什么结构?
    for (int i = 0; i < 9; i++)
    {
        for (int j = 0; j < 11; j++)
        {
            //if  else  switch
            switch (map[i][j])
            {
            case 0:
                printf("  ");
                break;
            case 1:
                printf("■");
                break;
            case 3:
                printf("☆");
                break;
            case 4:
                printf("□");
                break;
            case 5:
                printf("♀");  //5人
                break;
            case 7:     //4 + 3  箱子在目的地中
                printf("★");
                break;
            case 8:     // 5 + 3  人在目的地当中   人?
                printf("♀");
                break;
            }
        }
        printf("\n");
    }
}


void PlayGame()
{
    int r, c;  
    for (int i = 0; i < 9; i++)
    {
        for (int j = 0; j < 11; j++)
        {
            if (map[i][j] == 5||map[i][j]==8)   
            {
                r = i;
                c = j;
            }
        }
    }

    char ch;  
    ch = getch();  
    switch (ch)
    {
    case 'W':  
    case 'w':
    case 72:
        if (map[r - 1][c] == 0|| map[r - 1][c] == 3) 
        {
            map[r - 1][c] += 5;
            map[r][c] -= 5;
        }
        else if (map[r - 1][c] == 4 || map[r - 1][c] == 7)
        {
            if (map[r - 2][c] == 0 || map[r - 2][c] == 3)
            {
                map[r - 2][c] += 4;
                map[r - 1][c] += 1;
                map[r][c] -= 5;
            }
        }



        break;
